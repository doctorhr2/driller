#ifndef PLAYER_H_
#define PLAYER_H_

#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

using namespace Ogre;

// Player Class - Setting Hero Tank Entity with collision and physics

class Player
{
private:

	Entity* HeroTankEnt;								//Mesh
	SceneNode* HeroTankNode;							// Link to scene node
	Vector3 meshBoundingBox;							// Size of bounding mesh
	btCollisionShape* colShape;							// Collision boundary
	btRigidBody* body;									// Rigid body
	btDiscreteDynamicsWorld* discreteDynamics;			// World physics and collision

	float forwardForce;									//Move player forward
	float turningForce;
	btScalar linearDamping;
	btScalar angularDamping;

public:
	Player();
	~Player();

	void createMesh(SceneManager* scnMgr);				//Creates Player Mesh
	void attachToNode(SceneNode* parent);				//Creates Child of Parent Node
	void setScale(float x, float y, float z);			// Scale on all axis
	void setRotation(Vector3 axis, Radian angle);		// Sets Orientation
	void setPosition(float x, float y, float z);		// Set position

	void boundingBoxFromOgre();							//Taken from Tut - Bounding box
	void createRigidBody(float mass);					//New rigid body of given mass

	// Add to collison shape list
	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);

	//Add Rigid body to Dynamics  
	void addToDynamicsWorld(btDiscreteDynamicsWorld* discreteDynamics);

	void setMass(float mass);							//MFI wardrobe code						
	
	//According to Glenn, this makes coffee - about time!

	void update();

	void forward();

	void turnRight();

	void spinRight();

};

#endif