/*DRILLER DEMO - Ian Smith st20131102*/

/* Adapted from anim_1-12 demo, with Bullet commands introduced from Bulllet_Player demo. Some original code and comments may remain.*/

/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>
#include <iostream>
#include <OgreMeshManager.h>
#include "Game.h"
#include "OgreInput.h"
#include <OgreWindowEventUtilities.h>
#include <OgreRenderSystem.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "btIDebugDraw.h"
#include "al.h"
#include "alc.h"
#include "SoundManager.h"




Game::Game() : ApplicationContext("OgreTutorialApp")
{
		this->mLightAnimationState = 0;

		dynamicsWorld = NULL;

		wDown = false;
		aDown = false;
}


Game::~Game()
{
	//cleanup in the reverse order of creation/initialization
					//--CLEAN UP - Remove rigidbodies --//

	for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}

		dynamicsWorld->removeCollisionObject(obj);
		delete obj;
	}

	// Delete collison shapes
	for (int j = 0; j < collisionShapes.size(); j++)
		{
		btCollisionShape* shape = collisionShapes[j];
		collisionShapes[j] = 0;
		delete shape;
		}

	// Delete Discrete Dynamics
	delete dynamicsWorld;

	//Delete solver
	delete solver;

	//delete broadphase
	delete overlappingPairCache;

	//delete dispatcher
	delete dispatcher;

	delete collisionConfiguration;
}


void Game::setup()
{
	// do not forget to call the base first
	ApplicationContext::setup();

	addInputListener(this);


	// get a pointer to the already created root
	Root* root = getRoot();
	scnMgr = root->createSceneManager();

	// Initialise Resource Groups

	ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	// register our scene with the RTSS
	RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
	shadergen->addSceneManager(scnMgr);

	bulletInit();

	setupCamera();

	setupLights();

	//setupSound();

	setupCliffRoadMesh();

	setupCliffKerbMesh();

	setupPathLightMesh();

	setupRockLvl1Mesh();

	setupDoorRMesh();

	setupDoorLMesh();

	setupDoorPMesh();

	setupCrystalMesh();

	setupPlayer();

	setupFloor();

//	setupBoxMesh();

	//setupLightAnimation();
}

void Game::setupCamera()
{
	// Create Camera
	Camera* cam = scnMgr->createCamera("myCam");

	//Setup Camera
	cam->setNearClipDistance(5);

	// Position Camera - to do this it must be attached to a scene graph and added
	// to the scene.

	SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	 camNode->setPosition(0, 400, 900);										//Game Camera Angle
	 camNode->lookAt(Vector3(0, 200, -2500), Node::TransformSpace::TS_WORLD);
	//camNode->setPosition(0, 1000, 500);										//Debugging Camera Angle
	//camNode->lookAt(Vector3(0, -0, -0), Node::TransformSpace::TS_WORLD);
	camNode->attachObject(cam);

	// Setup viewport for the camera.

	Viewport* vp = getRenderWindow()->addViewport(cam);
	vp->setBackgroundColour(ColourValue(0, 0, 0));

	// link the camera and view port.
	cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
	// A quick Sky Dome. Pretty!
	scnMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);
}

void Game::bulletInit()
{
	collisionConfiguration = new btDefaultCollisionConfiguration();

	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	overlappingPairCache = new btDbvtBroadphase();

	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(0, -10, 0));

	
	//Debug Drawing
/*	dbgdraw = new BtOgre::DebugDrawer(scnMgr->getRootSceneNode(), dynamicsWorld);
	dynamicsWorld->setDebugDrawer(dbgdraw);
	dbgdraw->setDebugMode(bulletDebugDraw);
*/
}


void Game::setupPlayer()
{
	SceneNode* sceneRoot = scnMgr->getRootSceneNode();
	float mass = 1.0f;

	// Player Axis
	Vector3 axis(1.0, 1.0, 0.0);
	axis.normalise();

	//Player Angle
	Radian rads(Degree (0.0));

	player = new Player();
	player->createMesh(scnMgr);
	player->attachToNode(sceneRoot);

	player->setRotation(axis,rads);
	player->setPosition(20.0f, 20.0f, 20.0f);

	player->createRigidBody(mass);
	player->addToCollisionShapes(collisionShapes);
	player->addToDynamicsWorld(dynamicsWorld);
}
/*
void Game::setupBoxMesh()
{
	SceneNode* sceneRoot = scnMgr->getRootSceneNode();
	float mass = 1.0f;

	// Axis
	Vector3 axis(1.0, 1.0, 0.0);
	axis.normalise();

	//angle
	Radian rads(Degree(25.0));

	boxMesh = new BoxMesh();
	boxMesh->createMesh(scnMgr);
	boxMesh->attachToNode(sceneRoot);

	boxMesh->setRotation(axis, rads);
	boxMesh->setPosition(20.0f, 20.0f, 20.0f);

	boxMesh->createRigidBody(mass);
	boxMesh->addToCollisionShapes(collisionShapes);
	boxMesh->addToDynamicsWorld(dynamicsWorld);

}
*/
void Game::setupFloor()
{
	// Create a plane
	Plane plane(Vector3::UNIT_Y, -100);

	// Define the plane mesh
	MeshManager::getSingleton().createPlane(
		"ground", RGN_DEFAULT,
		plane,
		3000, 3000, 20, 20,
		true,
		1, 5, 5,
		Vector3::UNIT_Z);

	// Create an entity for the ground
	Entity* groundEntity = scnMgr->createEntity("ground");

	//Setup ground entity
	// Shadows off
	groundEntity->setCastShadows(false);

	// Material - Examples is the rsources file,
	// Rockwall (texture/properties) is defined inside it.
	groundEntity->setMaterialName("Examples/Rockwall");

	// Create a scene node to add the mesh too.
	SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	thisSceneNode->attachObject(groundEntity);

	//the ground is a cube of side 100 at position y = 0.
	   //the sphere will hit it at y = -6, with center at -5
	btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(3000.), btScalar(-50.), btScalar(3000.)));

	collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	//groundTransform.setOrigin(btVector3(0, -100, 0));

	Vector3 pos = thisSceneNode->_getDerivedPosition();

	//Box is 100 deep (dimensions are 1/2 heights)
	//but the plane position is flat.
	groundTransform.setOrigin(btVector3(pos.x, pos.y - 100, pos.z));
	//groundTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
	groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));


	btScalar mass(0.);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		groundShape->calculateLocalInertia(mass, localInertia);

	//using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	//   body->setRestitution(0.0);

	//add the body to the dynamics world
	dynamicsWorld->addRigidBody(body);
}
/*
void Game::setupLightAnimation()
{
	Light* alight = scnMgr->createLight("AnimLight");
	alight->setType(Light::LT_SPOTLIGHT);
	alight->setDiffuseColour(ColourValue(0.25f, 0.25f, 0.0f));
	alight->setSpecularColour(ColourValue(0.25f, 0.25f, 0.0f));
	alight->setAttenuation(8000, 1, 0.0005, 0);
	alight->setSpotlightRange(Ogre::Degree(60), Ogre::Degree(70));
	alight->setDirection(Vector3::NEGATIVE_UNIT_Y);

	BillboardSet* lightbillboardset = scnMgr->createBillboardSet("lightbbs", 1);
	lightbillboardset->setMaterialName("Examples/Flare");
	Billboard* lightbillboard = lightbillboardset->createBillboard(0, 0, 0, ColourValue(0.5, 0.3, 0.0f));

	SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode("AnimLightNode");
	lightNode->attachObject(alight);
	lightNode->attachObject(lightbillboardset);

	Real x = 20, y = 20, z = 100;

	lightNode->setPosition(x, y, z);
	Real s = 0.05f;
	lightNode->setScale(s, s, s);

	Real duration = 4.0;
	Real step = duration / 4.0;
	Animation* animation = scnMgr->createAnimation("LightAnim", duration);
	animation->setInterpolationMode(Animation::IM_SPLINE);
	NodeAnimationTrack* track = animation->createNodeTrack(0, lightNode);

	TransformKeyFrame* key;

	key = track->createNodeKeyFrame(0.0f);
	key->setTranslate(Vector3(-x, -y, z));
	key->setScale(Vector3(s, s, s));

	key = track->createNodeKeyFrame(step);
	key->setTranslate(Vector3(-x, y, z));
	key->setScale(Vector3(s, s, s));


	key = track->createNodeKeyFrame(2.0* step);
	key->setTranslate(Vector3(x, y, z));
	key->setScale(Vector3(s, s, s));


	key = track->createNodeKeyFrame(3.0* step);
	key->setTranslate(Vector3(x, -y, z));
	key->setScale(Vector3(s, s, s));


	key = track->createNodeKeyFrame(4.0* step);
	key->setTranslate(Vector3(-x, -y, z));
	key->setScale(Vector3(s, s, s));

	AnimationState * mLightAnimationState;

	mLightAnimationState = scnMgr->createAnimationState("LightAnim");
	mLightAnimationState->setEnabled(true);
	mLightAnimationState->setLoop(true);
}
*/

void Game::setupCliffRoadMesh()
{
	cliffRoadEnt = scnMgr->createEntity("drillROAD.mesh");
	cliffRoadEnt->setCastShadows(false);

	//cliffRoadNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, - 100, 300));
	cliffRoadNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, - 95, 0));
	cliffRoadNode->attachObject(cliffRoadEnt);

	cliffRoadNode->setScale(100, 100, 100);

	btCollisionShape*groundShape = new btBoxShape(btVector3(btScalar(5000.), btScalar(50.0), btScalar(5000.)));

	collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();

	Vector3 pos = cliffRoadNode->_getDerivedPosition();

	groundTransform.setOrigin(btVector3(pos.x, pos.y -100.0, pos.z));

	Quaternion quat2 = cliffRoadNode->_getDerivedOrientation();
	groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.w));

	btScalar mass(0.1);

	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		groundShape->calculateLocalInertia(mass, localInertia);

	btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
	btRigidBody::btRigidBodyConstructionInfo rbinfo(mass, myMotionState, groundShape, localInertia);
	btRigidBody* body = new btRigidBody(rbinfo);

	dynamicsWorld->addRigidBody(body);

}

void Game::setupCliffKerbMesh()
{
	cliffKerbEnt = scnMgr->createEntity("drillKERB.mesh");
	cliffKerbEnt->setCastShadows(false);

	cliffKerbNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, -100, 300));
	cliffKerbNode->attachObject(cliffKerbEnt);

	cliffKerbNode->setScale(100, 100, 100);
}

void Game::setupPathLightMesh()
{
	pathLightEnt = scnMgr->createEntity("drillGLASS.mesh");
	pathLightEnt->setCastShadows(false);

	pathLightNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, -100, 300));
	pathLightNode->attachObject(pathLightEnt);

	pathLightNode->setScale(100, 100, 100);
}

void Game::setupRockLvl1Mesh()
{
	rockLvl1Ent = scnMgr->createEntity("drillWallArena.mesh");
	rockLvl1Ent->setCastShadows(false);

	rockLvl1Node = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, -100, -1200));
	rockLvl1Node->attachObject(rockLvl1Ent);

	rockLvl1Node->setScale(100, 100, 100);
}

void Game::setupDoorRMesh()
{
	doorRightEnt = scnMgr->createEntity("drillDOOR.mesh");
	doorRightEnt->setCastShadows(false);

	doorRightNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(460, -100, -2675));
	doorRightNode->attachObject(doorRightEnt);

	doorRightNode->setScale(100, 100, 100);
}

void Game::setupDoorLMesh()
{
	doorLeftEnt = scnMgr->createEntity("drillDOORL.mesh");
	doorLeftEnt->setCastShadows(false);

	doorLeftNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(-460, -100, -2675));
	doorLeftNode->attachObject(doorLeftEnt);

	doorLeftNode->setScale(100, 100, 100);
}

void Game::setupCrystalMesh()
{
	crystalEnt = scnMgr->createEntity("drillCRYSTAL.mesh");
	crystalEnt->setCastShadows(false);

	crystalNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(260, -140, -2525));
	crystalNode->attachObject(crystalEnt);

	crystalNode->setScale(100, 100, 100);

	Entity* crystalEnt2 = scnMgr->createEntity("drillCRYSTAL.mesh");
	crystalEnt2->setCastShadows(false);
	SceneNode* crystalNode2 = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(-250, -140, - 2525));
	crystalNode2->attachObject(crystalEnt2);

	crystalNode2->setScale(100, 100, 100);
}

void Game::setupDoorPMesh()
{
	doorPortalEnt = scnMgr->createEntity("drillDOOR_UNLOCK.mesh");
	doorPortalEnt->setCastShadows(true);

	doorPortalNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(0, 0, -2500));
	doorPortalNode->attachObject(doorPortalEnt);

	doorPortalNode->setScale(100, 100, 100);
}
 //Assets

bool Game::frameRenderingQueued(const FrameEvent& evt)
{


	//mLightAnimationState->addTime(evt.timeSinceLastFrame);
	return true;
}

/*
void Game::setupSound()
{
	soundManagerPtr = new SoundManager;

	std::cout << soundManagerPtr->listAvailableDevices();

	soundManagerPtr->init();
	soundManagerPtr->setAudioPath((char*) ".\\");

	//TESTING PURPOSES

	unsigned int audioID;
	soundManagerPtr->loadAudio("darksolace.wav", &audioID, true);
	soundManagerPtr->playAudio(audioID, false);

}
*/

bool Game::frameStarted(const Ogre::FrameEvent &evt)
{
	//Be sure to call base class - otherwise events are not polled.
	ApplicationContext::frameStarted(evt);

	if (this->dynamicsWorld != NULL)
	{
		if (wDown)
			player->forward();

		if (aDown)
		{
			player->turnRight();
		}
	dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);

	//Update Object positions
	for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
		btRigidBody* body = btRigidBody::upcast(obj);
		btTransform trans;

		if (body && body->getMotionState())
		{
			body->getMotionState()->getWorldTransform(trans);

			void *userPointer = body->getUserPointer();

			if (userPointer == player)
			{
			}
			else
			{
				if (userPointer)
				{
					btQuaternion orientation = trans.getRotation();
					Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode *>(userPointer);
					sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
					sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
				}
			}
		}
		else
		{
			trans = obj->getWorldTransform();
		}
	}

//	this->dynamicsWorld->debugDrawWorld();

//	this->dbgdraw->step();

	player->update();

	}
	return true;

}

bool Game::frameEnded(const Ogre::FrameEvent &evt)
{
	if (this->dynamicsWorld != NULL)
	{
		dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);
	}
	return true;
}

void Game::setupLights()
{
	// Setup Ambient light
	scnMgr->setAmbientLight(ColourValue(0.5, 0.5, 0.5));
	scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

	// Add a spotlight
	Light* spotLight = scnMgr->createLight("SpotLight");

	// Configure
	spotLight->setDiffuseColour(0, 0, 1.0);
	spotLight->setSpecularColour(0, 0, 1.0);
	spotLight->setType(Light::LT_SPOTLIGHT);
	spotLight->setSpotlightRange(Degree(35), Degree(50));


	// Create a scene node for the spotlight
	SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	spotLightNode->setDirection(-1, -1, 0);
	spotLightNode->setPosition(Vector3(200, 200, 0));

	// Add spotlight to the scene node.
	spotLightNode->attachObject(spotLight);

	// Create directional light
	Light* directionalLight = scnMgr->createLight("DirectionalLight");

	// Configure the light
	directionalLight->setType(Light::LT_DIRECTIONAL);
	directionalLight->setDiffuseColour(ColourValue(0.4, 0, 0));
	directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));

	// Setup a scene node for the directional lightnode.
	SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	directionalLightNode->attachObject(directionalLight);
	directionalLightNode->setDirection(Vector3(0, -1, 1));

	// Create a point light
	Light* pointLight = scnMgr->createLight("PointLight");

	// Configure the light
	pointLight->setType(Light::LT_POINT);
	pointLight->setDiffuseColour(0.3, 0.3, 0.3);
	pointLight->setSpecularColour(0.3, 0.3, 0.3);

	// setup the scene node for the point light
	SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

	// Configure the light
	pointLightNode->setPosition(Vector3(0, 150, 250));

	// Add the light to the scene.
	pointLightNode->attachObject(pointLight);

}



bool Game::keyPressed(const KeyboardEvent& evt)
{
	//  std::cout << "Got key event" << std::endl;
	if (evt.keysym.sym == SDLK_ESCAPE)
	{
		getRoot()->queueEndRendering();
	}
	if (evt.keysym.sym == 'w')
	{
		wDown = true;
	}
	if (evt.keysym.sym == 'd')
	{
		aDown = true;
	}
	/*
	if (evt.keysym.sym == 'f')
	{
		bulletDebugDraw = !bulletDebugDraw;
		this->dbgdraw->setDebugMode(bulletDebugDraw);
	}
	*/
	return true;
}


bool Game::keyReleased(const KeyboardEvent& evt)
{
	std::cout << "Key Up" << std::endl;

	if (evt.keysym.sym == 'w')
	{
		wDown = false;
	}

	if (evt.keysym.sym == 'd')
	{
		aDown = false;
	}

	return true;
}

bool Game::mouseMoved(const MouseMotionEvent& evt)
{
	//	std::cout << "Got Mouse" << std::endl;
	return true;
}
