#ifndef BOXMESH_H_
#define BOXMESH_H_

#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

using namespace Ogre;

class BoxMesh
{
private:
	SceneNode* boxMeshSceneNode;
	Entity* boxMesh;
	Vector3 meshBoundingBox;

	btCollisionShape* colShape;
	btRigidBody* body;
	btDiscreteDynamicsWorld* dynamicsWorld;


public:
	BoxMesh();
	~BoxMesh();

	// Create Mesh
	void createMesh(SceneManager* scnMgr);
	void attachToNode(SceneNode* parent);
	void setScale(float x, float y, float z);
	void setRotation(Vector3 axis, Radian angle);
	void setPosition(float x, float y, float z);

	void boundingBoxFromOgre();

	void createRigidBody(float mass);

	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
	void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);

	void setMass(float mass);

	void update();
};

#endif // !BOXMESH_H_
