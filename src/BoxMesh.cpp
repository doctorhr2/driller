#include "BoxMesh.h"



BoxMesh::BoxMesh()
{
	boxMeshSceneNode = nullptr;
	boxMesh = nullptr;
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);

	colShape = nullptr;
	dynamicsWorld = nullptr;
}


BoxMesh::~BoxMesh()
{
}

void BoxMesh::createMesh(SceneManager * scnMgr)
{
	boxMesh->setCastShadows(true);
	boxMesh = scnMgr->createEntity("cube.mesh");
}

void BoxMesh::attachToNode(SceneNode* parent)
{
	
	boxMeshSceneNode = parent->createChildSceneNode();
	boxMeshSceneNode->attachObject(boxMesh);
	boxMeshSceneNode->setScale(1.0f, 1.0f, 1.0f);
	boundingBoxFromOgre();
}
void BoxMesh::setScale(float x, float y, float z)
{
	boxMeshSceneNode->setScale(x, y, z);
}

void BoxMesh::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis);
	boxMeshSceneNode->setOrientation(quat);
}

void BoxMesh::setPosition(float x, float y, float z)
{
	boxMeshSceneNode->setPosition(x, y, z);
}

void BoxMesh::boundingBoxFromOgre()
{
	boxMeshSceneNode->_updateBounds();
	const AxisAlignedBox& b = boxMeshSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}

void BoxMesh::createRigidBody(float bodymass)
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));

	btTransform startTranform;
	startTranform.setIdentity();

	Quaternion quat2 = boxMeshSceneNode->_getDerivedOrientation();
	startTranform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = boxMeshSceneNode->_getDerivedPosition();
	startTranform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(1.f);

	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTranform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	body->setUserPointer((void*)boxMeshSceneNode);

	dynamicsWorld->addRigidBody(body);

}
