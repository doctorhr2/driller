#include "Player.h"


Player::Player()
{
	HeroTankNode = nullptr;
	HeroTankEnt = nullptr;
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);

	colShape = nullptr;
	discreteDynamics = nullptr;

	forwardForce = -100.0f;
	//backForce = 100.0f
	turningForce = 20.0f;
	linearDamping = 0.8f;
	angularDamping = 0.8f;
}

Player::~Player()
{

}

void Player::createMesh(SceneManager * scnMgr)
{
	HeroTankEnt = scnMgr->createEntity("drillTANK.mesh");
	HeroTankEnt->setCastShadows(false);							//Shadows not working on Tank. Possibly Ogre can't handle open faces on imported 3Ds meshes. 
																//Cannot handle anything but single meshes - HUGE issue. 
}

void Player::attachToNode(SceneNode* parent)
{
	HeroTankNode = parent->createChildSceneNode();
	HeroTankNode->attachObject(HeroTankEnt);
	HeroTankNode->setScale(100, 100, 100);
	boundingBoxFromOgre();
}

void Player::setScale(float x, float y, float z)
{
	HeroTankNode->setScale(x, y, z);
}

void Player::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis);
	HeroTankNode->setOrientation(quat);
}

void Player::setPosition(float x, float y, float z)
{
	HeroTankNode->setPosition(25, -50, z);
}

void Player::boundingBoxFromOgre()
{
	HeroTankNode->_updateBounds();
	const AxisAlignedBox& b = HeroTankNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}
void Player::createRigidBody(float bodyMass)
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));

	//Create Dynamic Objects - Dynamically!
	btTransform startTransform;
	startTransform.setIdentity();

	Quaternion quat2 = HeroTankNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z));

	Vector3 pos = HeroTankNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(bodyMass);

	bool isDynamic = (mass != 0.f); // rigidbody is dynamic if mass is not zero, otherwise static

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		// FOR DEBUGGING PURPOSES ADD:
		//std::cout << "The Cube is Dynamic" << std::endl;
		colShape->calculateLocalInertia(mass, localInertia); 
	}

	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	body = new btRigidBody(rbInfo);

	//Might be better to use friction rather than linear and angular, as the tank doesn't hover - I'll get it moving first!
	body->setDamping(linearDamping, angularDamping);
	
	body->setUserPointer((void*)this);
}
void Player::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void Player::addToDynamicsWorld(btDiscreteDynamicsWorld* discreteDynamics)
{
	this->discreteDynamics = discreteDynamics;
	discreteDynamics->addRigidBody(body);
}

void Player::setMass(float mass)
{
}

void Player::update()
{
	btTransform trans;

	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		HeroTankNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
		HeroTankNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
	}
}

void Player::forward()
{
	//Creates a vector in local coordinates - pointing down Z
	btVector3 fwd(0.0f, 0.0f, forwardForce);
	btVector3 push;

	btTransform trans;

	if (body && body->getMotionState())
	{
		// Get orientation of rigidbody in worldspace
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		// Rotate local force to global space (push down in local Z)
		push = quatRotate(orientation, fwd);

		//Activate the body if it has stopped moving
		body->activate();

		//Apply force to centre of body
		body->applyCentralForce(push);
	}
}

void Player::turnRight()
{
	btVector3 right(turningForce, 0.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		btVector3 front(trans.getOrigin());

		front += btVector3(0.0f, 0.0f, meshBoundingBox.z / 2);

		turn = quatRotate(orientation, right);

		if (body->getLinearVelocity().length() > 0.0f)
			body->applyForce(turn, front);
	}
}

void Player::spinRight()
{
	btVector3 right(0.0f, 100.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (body && body->getMotionState())
	{
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		turn = quatRotate(orientation, right);

		body->activate();

		body ->applyTorque(right);
	}
}