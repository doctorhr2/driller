#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"
#include "OgreApplicationContext.h"
#include "OgreCameraMan.h"
#include "OgreMeshManager.h"
#include <OgreWindowEventUtilities.h>
#include <OgreRenderSystem.h>

#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"

using namespace Ogre;
using namespace OgreBites;

#include "Player.h"
//#include "BoxMesh.h" - Currently not working

class Game : public ApplicationContext, public InputListener
{
private:
	SceneManager* scnMgr;

	// Collision stuff

	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();	//Used to for memory allocation
	
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);				// Used to handle collision pairs, time of impact, penetration depth
	
	btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();								// Detects overlapping pairs aabb
	
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;				//Quick way to solve linear equations with unknown variables
	
	btDiscreteDynamicsWorld* dynamicsWorld;															// Adds Physics and Collision (p.12 Bullet Users Manual)

	btAlignedObjectArray<btCollisionShape*> collisionShapes;											//Keeps tracks of shapes and relases on exit


	//SoundManager* soundManagerPtr = new SoundManager;			// Pointer to Sound Manager

	//Debugging
	/*
	BtOgre::DebugDrawer *dbgdraw;

	bool ogreDebugDraw;
	bool bulletDebugDraw;
	*/
	Player *player;				//Player Tank

//	BoxMesh *boxMesh;			//Box to test collision

	bool wDown;					// Putting this here = don't know enough to argue why not!

	bool aDown;

	

	//bool zDown;

	//bool dDown;

	// Asset setup


	

	Entity* cliffRoadEnt;
	SceneNode* cliffRoadNode;
	
	Entity* doorLeftEnt;
	SceneNode* doorLeftNode;

	Entity* crystalEnt;
	SceneNode* crystalNode;

	Entity* doorPortalEnt;
	SceneNode* doorPortalNode;

	Entity* doorRightEnt;
	SceneNode* doorRightNode;

	Entity* cliffKerbEnt;
	SceneNode* cliffKerbNode;

	Entity* pathLightEnt;
	SceneNode* pathLightNode;

	Entity* rockLvl1Ent;
	SceneNode* rockLvl1Node;


	AnimationState*  mLightAnimationState;

public:
	Game();
	virtual ~Game();

	void setup();

	void setupCamera();

	void setupFloor();

	//void setupLightAnimation();

	void setupPlayer();
	void setupCliffRoadMesh();
	void setupCliffKerbMesh();
	void setupPathLightMesh();
	void setupCrystalMesh();
	void setupDoorRMesh();
	void setupDoorLMesh();
	void setupDoorPMesh();
	void setupRockLvl1Mesh();
	//void setupBoxMesh();
	void setupLights();
	//void setupSound();

	bool keyPressed(const KeyboardEvent& evt);
	bool keyReleased(const KeyboardEvent & evt);
	bool mouseMoved(const MouseMotionEvent& evt);
	bool frameStarted(const FrameEvent &evt);
	bool frameEnded(const FrameEvent &evt);

	//This is new!
	bool frameRenderingQueued(const FrameEvent& evt);

	void bulletInit();

};
